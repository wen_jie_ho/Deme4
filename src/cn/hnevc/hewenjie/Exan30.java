package cn.hnevc.hewenjie;

public class Exan30 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[][] array = new int[3][];// 定义一个长度为三的二维数组
		array[0] = new int[] { 11, 12, };
		array[1] = new int[] { 21, 22, 23 };
		array[2] = new int[] { 31, 32, 33, 34 };
		int sum = 0;
		for (int i = 0; i < array.length; i++) {
			int groupSum = 0;
			for (int j = 0; j < array[i].length; j++) {
				groupSum = groupSum + array[i][j];// 每个小组成员销售额相加
			}
			sum = sum + groupSum;
			System.out.println("第" + (i + 1) + "小组销售额：" + groupSum + "万元");
		}
		System.out.println("总销售额为：" + sum + "万元");
	}

}
