package cn.hnevc.quyongjun;

import java.util.Random;
import java.util.Scanner;

public class Example31 {
	public static void main(String[] args) {
		String[] students = new String[3];
		addStudentName(students);// 存储全班同学姓名
		printStudentName(students);// 总览全班同学姓名
		String name = randomStudentName(students);// 随机点名其中一个人
		System.out.println("被点名的同学是" + name);
	}

	// 存储全部同学的名字的方法
	public static void addStudentName(String[] students) {
		// 创建学生姓名数组容器
		Scanner scanner = new Scanner(System.in);
		for (int i = 0; i < students.length; i++) {
			System.out.println("存储第" + (i + 1) + "个姓名:");
			students[i] = scanner.next();// 通过键盘输入姓名存入数组中
		}
	}

	// 总览全部同学姓名
	public static void printStudentName(String[] students) {
		for (int i = 0; i < students.length; i++) {
			String name = students[i];
			System.out.println("第" + (i + 1) + "个学生姓名" + name);

		}
	}

	// 根据随机数随即点名
	public static String randomStudentName(String[] students) {
		int index = new Random().nextInt(students.length);
		String name = students[index];
		return name;
	}
}
